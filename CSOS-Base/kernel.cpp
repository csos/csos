#ifndef __cplusplus
#include <stdbool.h>
#endif
#include "kernel.hpp"

#ifdef __linux__
#error "You are not using an ix86-elf cross-compiler! Prepare for failfest!"
#endif

#ifndef __i386__
#error "You are not using an ix86-elf cross-compiler! Prepare for failfest!"
#endif

uint8_t make_color(enum vga_color fg, enum vga_color bg) {
	return fg | bg << 4;
}

uint16_t make_vgaentry(char c, uint8_t color) {
	uint16_t c16 = c;
	uint16_t color16 = color;
	return c16 | color16 << 8;
}

size_t strlen(const char* str) {
	size_t ret = 0;
	while (str[ret] != 0) {
		ret++;
	}
	return ret;
}

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

void terminal_clear() {
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = make_vgaentry(' ', terminal_color);
		}
	}
}

void terminal_initialize() {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_WHITE, COLOR_BLACK);
	terminal_buffer = (uint16_t*)0xB8000;
	terminal_clear();
}

void terminal_setcolor(uint8_t color) {
	terminal_color = color;
	terminal_clear();
}

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}

void terminal_putchar(char c) {
	if (c == '\n') {
		terminal_column = 0;
		terminal_row++;
	}
	else {
		terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
		if (++terminal_column == VGA_WIDTH) {
			terminal_column = 0;
			terminal_row++;
		}
		if (++terminal_row == VGA_HEIGHT) {
			terminal_clear();
			terminal_column = 0;
			terminal_row = 0;
		}
	}
}

void printf(const char* data) {
	size_t datalen = strlen(data);
	for (size_t i = 0; i < datalen; i++) terminal_putchar(data[i]);
}

#ifdef __cplusplus
extern "C"
#endif

void kernel_main() {
	terminal_initialize();
	terminal_setcolor(make_color(COLOR_BLUE, COLOR_RED));
	printf("Registering ATA Kernext");
	printf("[KERNEL PANIC]\nOh, noes!\nCSOS has crashed:\nYour CSOS installation has been damaged.");
	make_color(COLOR_WHITE, COLOR_BLACK);
}