#ifndef __KERNEL_HPP__
#define __KERNEL_HPP__

namespace CSOSKernel {
	enum kernexttype_t {
		DRIVER,
		BOOT,
		DEFAULT
	};
	class kernext {
	public:
		kernexttype_t type = DEFAULT;
		bool usesASM = false;
	};
}

#endif